# -*- coding: utf-8 -*-

import cv2
import numpy as np
import __init__ as my_s
import matplotlib.pyplot as plt

height1 = 480
width1 = 720
height2 = 480
width2 = 640

cv2.namedWindow('Image')

cap = my_s.cought_camers(2, 0, height1, width1, height2, width2)

while True:
    ret1, frame1 = cap[0].read()
    assert ret1  # succeeds
    ret2, frame2 = cap[1].read()
    assert ret2  # fails?!

    frame2copy = frame2.copy()
    frame2 = frame2[170 : 326, 230 : 435 ]
    frame2 = cv2.resize(frame2, frame2copy.shape[:2][:: -1])
    frame = my_s.merge_frame(frame2, frame1)

    cv2.imshow('Image', frame)

    if 27 == cv2.waitKey(30):
        break