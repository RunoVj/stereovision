import cv2
import numpy as np
import os
import datetime
from time import gmtime, strftime

def cought_camers(num1, num2, height1, width1, height2, width2):
    cap = []
    cap.append(cv2.VideoCapture(num1))
    cap[0].set(3, width1)
    cap[0].set(4, height1)
    cap.append(cv2.VideoCapture(num2))
    cap[1].set(3, width2)
    cap[1].set(4, height2)
    return cap

def is_non_zero_file(fpath):
    return os.path.isfile(fpath) and os.path.getsize(fpath) > 0

def rotateVideo(cam1, cam2):
    # low-up rotation
    #rotationCam0 = cv2.flip(cam0, 1)

# mirror (horizontal and vertical rotation)
    rotationCam1 = cv2.flip(cam1, 1)
    small1 = cv2.resize(rotationCam1, (0, 0), fx=0.31, fy=0.31)


# 90 degree rotation
    dect = np.zeros((0, 0, 3), np.uint8)
    cam2 = cv2.transpose(cam2, dect)
    rotationCam2 = cv2.flip(cam2, 0)
    rotationCam2 = cv2.flip(rotationCam2, 1)
    small2 = cv2.resize(rotationCam2, (0, 0), fx=1, fy=0.8)
    small2 = rotationCam2[228:378, 135:335] # left top right bottom 250 400 100 300 --- 235: 375 220:420
    #small2 = cv2.resize(rotationCam2, (0, 0), fx=1, fy=0.8)

    #return rotationCam0, rotationCam1
    return small1, small2

def merge_frame(img1, img2):
    if img1.shape[0] != img2.shape[0]:
        img2 = cv2.resize(img2, img1.shape[:2][::-1])
    img = np.zeros((img1.shape[0], img1.shape[1] + img2.shape[1], 3), np.uint8)
    img[0: img1.shape[0], 0: img1.shape[1]] = img1
    img[0 : img1.shape[0], img1.shape[1]: img1.shape[1] + img2.shape[1]] = img2

    return img

def get_right_left_from_img(img):
    img_left = img[0 : img.shape[0], 0 : img.shape[1] / 2] #Y+H and X+W
    img_right = img[0 : img.shape[0], img.shape[1] / 2 : img.shape[1] ]
    return img_left, img_right
