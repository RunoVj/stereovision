import cv2
import numpy as np


def get_image(camera):
    # read is the easiest way to get a full image out of a VideoCapture object.
    retval, im = camera.read()
    return retval, im


def save_chessImg(camera, path):
    ramp_frames = 130
    for i in xrange(ramp_frames):
        cv2.imwrite(path + str(i) + ".png", camera)
    print("Taking image...")


def rotateVideo(cam0, cam1):
    #cam1 - hd 3 -only  0 - web was 2 - hd
    # low-up rotation
    #rotationCam0 = cv2.flip(cam0, 1)

# mirror (horizontal and vertical rotation)
    rotationCam0 = cv2.flip(cam0, 1)
    small0 = cv2.resize(rotationCam0, (0, 0), fx=0.31, fy=0.31)


# 90 degree rotation
    dect = np.zeros((0, 0, 3), np.uint8)
    cam1 = cv2.transpose(cam1, dect)
    rotationCam1 = cv2.flip(cam1, 0)
    rotationCam1 = cv2.flip(rotationCam1, 1)
    small1 = rotationCam1[234:374, 185:385] # left top right bottom 250 400 100 300 --- 235: 375 220:420
   # small1 = cv2.resize(rotationCam1, (0, 0), fx=1, fy=0.8)

    return small0, small1


#main
def main():
    cap0 = cv2.VideoCapture(1)
    cap0.set(3, 720)
    cap0.set(4, 480)

    cap1 = cv2.VideoCapture(1)
    cap1.set(3, 640)
    cap1.set(4, 480)

    file0Path = ("D:\\CameraHD\\test_image") #usb 1
    file1Path = ("D:\\Camera\\test_image") # web 2
    ret0, frame0 = get_image(cap0)
    assert ret0

    ret1, frame1 = get_image(cap1)
    assert ret1

    frame0, frame1 = rotateVideo(frame0, frame1)

    save_chessImg(frame0, file0Path)
    save_chessImg(frame1, file1Path)


    del (frame0)
    del (frame1)

if __name__ == '__main__':
	main()

'''
while True:
    ret0, frame0 = get_image(cap0)
    assert ret0

    ret1, frame1 = get_image(cap1)
    assert ret1

    frame0, frame1 = rotateVideo(frame0, frame1)

    save_chessImg(frame, file0Path)
'''

'''
    cv2.resizeWindow('frame0', 480, 360) #480 360
    cv2.resizeWindow('frame1', 480, 360)
    cv2.imshow('frame0', frame0)
    cv2.imshow('frame1', frame1)

    pressed_key = cv2.waitKey(30)
    if pressed_key == 27:
        break
'''