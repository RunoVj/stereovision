from __future__ import print_function

import numpy as np
import cv2
import __init__ as my_s

# local modules
from common import splitfn

# built-in modules
import os

if __name__ == '__main__':
    import sys
    import getopt
    from glob import glob

    MAT_SIZE = 3

    RMS1, RMS2 = 0, 0
    K1, K2 = np.zeros((MAT_SIZE, MAT_SIZE)), np.zeros((MAT_SIZE, MAT_SIZE))
    dist_coef1, dist_coef2 = np.zeros([MAT_SIZE]), np.zeros([MAT_SIZE])

    if my_s.is_non_zero_file('calibration.txt'):
        with open('calibration.txt', 'r') as file:
            RMS1 = float(file.readline())
            for i in range(MAT_SIZE):
                K1[i] = (np.array([float(word) for word in file.readline().split(' ')]))
            dist_coef1 = np.array([float(word) for word in file.readline().split(' ')])

            RMS2 = float(file.readline())
            for i in range(MAT_SIZE):
                K2[i] = (np.array([float(word) for word in file.readline().split(' ')]))
            dist_coef2 = np.array([float(word) for word in file.readline().split(' ')])

            # print(RMS1)
            # print(K1)
            # print(dist_coef1)
            # print(RMS1)
            # print(K1)
            # print(dist_coef1)

            # else:
            # run calibration.py

    args, img_mask = getopt.getopt(sys.argv[1:], '', ['debug=', 'square_size='])
    args = dict(args)
    args.setdefault('--debug', './output/')
    args.setdefault('--square_size', 1.0)
    if not img_mask:
        img_mask = '../data/stereo*.png'  # default
    else:
        img_mask = img_mask[0]

    img_names = glob(img_mask)
    debug_dir = args.get('--debug')
    if not os.path.isdir(debug_dir):
        os.mkdir(debug_dir)
    square_size = float(args.get('--square_size'))

    pattern_size = (5, 4)
    pattern_points = np.zeros((np.prod(pattern_size), 3), np.float32)
    pattern_points[:, :2] = np.indices(pattern_size).T.reshape(-1, 2)
    pattern_points *= square_size

    obj_points = []
    #img_points = []
    img_points_left = []
    img_points_right = []
    h, w = 0, 0
    for fn in img_names:
        print('processing %s... ' % fn, end='')
        img = cv2.imread(fn, 0)
        if img is None:
            print("Failed to load", fn)
            continue
        img_left, img_right = my_s.get_right_left_from_img(img)
        h, w = img.shape[:2]
        found_left, corners_left = cv2.findChessboardCorners(img_left, pattern_size)
        found_right, corners_right = cv2.findChessboardCorners(img_right, pattern_size)
        if found_left and found_right:
            term = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.1)

            cv2.cornerSubPix(img, corners_left, (5, 5), (-1, -1), term)
            cv2.cornerSubPix(img, corners_right, (5, 5), (-1, -1), term)

        if debug_dir:
            vis_left = cv2.cvtColor(img_left, cv2.COLOR_GRAY2BGR)
            vis_right = cv2.cvtColor(img_right, cv2.COLOR_GRAY2BGR)
            cv2.drawChessboardCorners(vis_left, pattern_size, corners_left, found_left and found_right)
            cv2.drawChessboardCorners(vis_right, pattern_size, corners_right, found_left and found_right)
            path, name, ext = splitfn(fn)
            outfile_left = debug_dir + name + 'left_chess.jpg'
            outfile_right = debug_dir + name + 'right_chess.jpg'
            cv2.imwrite(outfile_left, vis_left)
            cv2.imwrite(outfile_left, vis_right)

        if not (found_left and found_right):
            print('chessboard not found')
            continue

        img_points_left.append(corners_left.reshape(-1, 2))
        img_points_right.append(corners_left.reshape(-1, 2))

        obj_points.append(pattern_points)

        print('ok')

    # calculate camera distortion
    #rms, camera_matrix, dist_coefs, rvecs, tvecs = cv2.calibrateCamera(obj_points, img_points, (w, h), None, None, flags = cv2.CALIB_RATIONAL_MODEL)

    retval, cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, R, T, E, F = cv2.stereoCalibrate(obj_points, img_points_left, img_points_right,
                                                                                                     K1, dist_coef1, K2, dist_coef2, (w * 2, h))

    print('retval : ', retval)
    print('cameraMatrix1 : ',cameraMatrix1)
    print('cameraMatrix2 : ',cameraMatrix2)
    print('distCoeffs1 : ',distCoeffs1)
    print('distCoeffs2 : ',distCoeffs2)
    print('R : ',R)
    print('T : ',T)
    print('E : ',E)
    print('F : ',F)

    cv2.destroyAllWindows()
